from django.db import models

class DogBreed(models.Model):
    name = models.CharField(max_length=255)
    intelligence = models.IntegerField()
    stamina = models.IntegerField()
    size = models.IntegerField()