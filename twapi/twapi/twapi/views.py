from rest_framework import viewsets, filters
from .models import DogBreed
from .serializers import DogBreedSerializer

class DogBreedViewSet(viewsets.ModelViewSet):
    queryset = DogBreed.objects.all()
    serializer_class = DogBreedSerializer
    filter_backends = (filters.OrderingFilter,)
    ordering_fields = "__all__"
