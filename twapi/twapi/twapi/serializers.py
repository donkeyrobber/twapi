from .models import DogBreed
from rest_framework import serializers

class DogBreedSerializer(serializers.ModelSerializer):
    name = serializers.CharField(max_length = 255)
    intelligence = serializers.IntegerField()
    stamina = serializers.IntegerField()
    size = serializers.IntegerField()

    class Meta:
        model = DogBreed
        fields = "__all__"